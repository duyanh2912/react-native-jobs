import {Navigation} from "react-native-navigation";
import {registerScreens} from "./screens";
import stores from "./stores";
import Provider from "./utils/MobxRnnProvider"
import {LOGIN_SCREEN} from "./constants/screens";
import DefaultStyles from "./styles";

registerScreens(stores, Provider);

Navigation.startSingleScreenApp({
    screen: {
        screen: LOGIN_SCREEN,
        title: "Đăng nhập",
        navigatorStyle: DefaultStyles.navigatorStyle
    }
});