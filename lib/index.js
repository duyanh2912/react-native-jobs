"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const screens_1 = require("./screens");
const stores_1 = require("./stores");
const MobxRnnProvider_1 = require("./utils/MobxRnnProvider");
const React = require("react");
class App extends React.Component {
}
App.start = () => {
    stores_1.getStores().then(async (stores) => {
        screens_1.registerScreens(stores, MobxRnnProvider_1.default);
        if (!stores.FB_AUTH_STORE.accessToken) {
            // startAppWithWelcomeScreen();
            await screens_1.startAppWithMainScreens();
        }
        else {
            await screens_1.startAppWithMainScreens();
        }
    });
};
App.start();
//# sourceMappingURL=index.js.map