"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_react_1 = require("mobx-react");
const React = require("react");
const mobx_1 = require("mobx");
const react_native_1 = require("react-native");
const react_native_elements_1 = require("react-native-elements");
const StoreNames_1 = require("../constants/StoreNames");
const JobSearchMapView_1 = require("../components/JobSearchMapView");
const Colors_1 = require("../constants/Colors");
const Codepush_1 = require("../constants/Codepush");
const codePush = require("react-native-code-push");
let MapScreen = class MapScreen extends React.Component {
    constructor() {
        super(...arguments);
        this.isMapLoaded = false;
        this.isFetchingJobs = false;
        this.initialRegion = {
            longitude: -122,
            latitude: 37,
            longitudeDelta: 0.04,
            latitudeDelta: 0.09,
        };
        this.onRegionChangeComplete = (region) => {
            this.currentRegion = region;
        };
        this.onButtonPress = async () => {
            try {
                this.isFetchingJobs = true;
                await this.jobStore.getJobsFromCoordinate(this.currentRegion);
                this.props.navigator.switchToTab({ tabIndex: 1 });
            }
            catch (err) {
                alert(err.message);
            }
            finally {
                this.isFetchingJobs = false;
            }
        };
    }
    get jobStore() {
        return this.props[StoreNames_1.default.JobStore];
    }
    componentDidMount() {
        this.isMapLoaded = true;
    }
    render() {
        if (!this.isMapLoaded) {
            return (<react_native_1.View style={{ flex: 1, justifyContent: "center" }}>
                    <react_native_1.ActivityIndicator size="large"/>
                </react_native_1.View>);
        }
        return (<react_native_1.View style={{ flex: 1 }}>
                <JobSearchMapView_1.default style={{ flex: 1 }} initialRegion={this.initialRegion} onRegionChangeComplete={this.onRegionChangeComplete} jobs={this.jobStore.jobs}/>
                <react_native_elements_1.Button large={true} title="Search this area" backgroundColor={Colors_1.default.Primary} icon={{ name: "search" }} onPress={this.onButtonPress} containerViewStyle={styles.buttonContainer} disabled={this.isFetchingJobs}/>
            </react_native_1.View>);
    }
};
__decorate([
    mobx_1.observable
], MapScreen.prototype, "isMapLoaded", void 0);
__decorate([
    mobx_1.observable
], MapScreen.prototype, "isFetchingJobs", void 0);
__decorate([
    mobx_1.observable
], MapScreen.prototype, "initialRegion", void 0);
__decorate([
    mobx_1.observable
], MapScreen.prototype, "currentRegion", void 0);
MapScreen = __decorate([
    codePush(Codepush_1.CODE_PUSH_OPTIONS),
    mobx_react_1.inject(StoreNames_1.default.JobStore),
    mobx_react_1.observer
], MapScreen);
const styles = react_native_1.StyleSheet.create({
    buttonContainer: {
        position: "absolute",
        bottom: 20,
        left: 16,
        right: 16
    }
});
exports.default = MapScreen;
//# sourceMappingURL=MapScreen.js.map