"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_react_1 = require("mobx-react");
const React = require("react");
const StoreNames_1 = require("../constants/StoreNames");
const Deck_1 = require("../components/Deck");
const JobCard_1 = require("../components/JobCard");
const react_native_elements_1 = require("react-native-elements");
const Colors_1 = require("../constants/Colors");
let DeckScreen = class DeckScreen extends React.Component {
    constructor() {
        super(...arguments);
        this.renderNoMoreCard = () => {
            return (<react_native_elements_1.Card title="No more jobs">
                <react_native_elements_1.Button large={true} title="Back To Map" icon={{ name: "my-location" }} backgroundColor={Colors_1.default.Primary} onPress={() => this.props.navigator.switchToTab({ tabIndex: 0 })}/>
            </react_native_elements_1.Card>);
        };
        this.renderJobCards = (job) => {
            return <JobCard_1.default job={job}/>;
        };
        this.addJobToLiked = (job) => {
            // Push action to the end of the queue so the animation can run first
            setTimeout(() => this.jobStore.addJobToLiked(job), 1);
        };
    }
    get jobStore() {
        return this.props[StoreNames_1.default.JobStore];
    }
    render() {
        return (<Deck_1.default data={this.jobStore.jobs} keyExtractor={(job) => job.jobkey} renderCard={this.renderJobCards} renderNoMoreCard={this.renderNoMoreCard} onSwipeRight={(job) => this.addJobToLiked(job)} containerStyle={{ flex: 1, backgroundColor: "white" }}/>);
    }
};
DeckScreen = __decorate([
    mobx_react_1.inject(StoreNames_1.default.JobStore),
    mobx_react_1.observer
], DeckScreen);
exports.default = DeckScreen;
//# sourceMappingURL=DeckScreen.js.map