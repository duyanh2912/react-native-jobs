"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_react_1 = require("mobx-react");
const React = require("react");
const react_native_1 = require("react-native");
const react_native_fbsdk_1 = require("react-native-fbsdk");
let AuthScreen = class AuthScreen extends React.Component {
    componentWillMount() {
        react_native_fbsdk_1.LoginManager.logInWithReadPermissions(["public_profile"]).then((result) => {
            if (result.isCancelled) {
                alert("Login cancelled");
            }
            else {
                alert("Login success with permissions: "
                    + result.grantedPermissions.toString());
            }
        }, (error) => {
            alert("Login fail with error: " + error);
        });
    }
    render() {
        return <react_native_1.Text>Auth</react_native_1.Text>;
    }
};
AuthScreen = __decorate([
    mobx_react_1.observer
], AuthScreen);
exports.default = AuthScreen;
//# sourceMappingURL=AuthScreen.js.map