"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const WelcomeScreen_1 = require("./WelcomeScreen");
const MapScreen_1 = require("./MapScreen");
const react_native_navigation_1 = require("react-native-navigation");
const ScreenNames_1 = require("../constants/ScreenNames");
const FontAwesome_1 = require("react-native-vector-icons/dist/FontAwesome");
const DeckScreen_1 = require("./DeckScreen");
const ReviewScreen_1 = require("./ReviewScreen");
const SettingScreen_1 = require("./SettingScreen");
const Colors_1 = require("../constants/Colors");
const screens = {
    [ScreenNames_1.ScreenNames.Welcome]: WelcomeScreen_1.default,
    [ScreenNames_1.ScreenNames.Map]: MapScreen_1.default,
    [ScreenNames_1.ScreenNames.Deck]: DeckScreen_1.default,
    [ScreenNames_1.ScreenNames.Review]: ReviewScreen_1.default,
    [ScreenNames_1.ScreenNames.Setting]: SettingScreen_1.default
};
function registerScreens(store, Provider) {
    Object.keys(screens).forEach(key => {
        react_native_navigation_1.Navigation.registerComponent(key, () => screens[key], store, Provider);
    });
}
exports.registerScreens = registerScreens;
function startAppWithWelcomeScreen() {
    react_native_navigation_1.Navigation.startSingleScreenApp({
        screen: {
            screen: ScreenNames_1.ScreenNames.Welcome,
            navigatorStyle: {
                navBarHidden: true
            }
        }
    });
}
exports.startAppWithWelcomeScreen = startAppWithWelcomeScreen;
async function startAppWithMainScreens() {
    react_native_navigation_1.Navigation.startTabBasedApp({
        tabs: [
            {
                label: "Location",
                screen: ScreenNames_1.ScreenNames.Map,
                navigatorStyle: {
                    navBarHidden: true
                },
                icon: await FontAwesome_1.default.getImageSource("map-o", 22),
                selectedIcon: await FontAwesome_1.default.getImageSource("map", 22)
            }, {
                label: "Rate",
                screen: ScreenNames_1.ScreenNames.Deck,
                icon: await FontAwesome_1.default.getImageSource("star-o", 22),
                selectedIcon: await FontAwesome_1.default.getImageSource("star", 22),
                title: "Rate",
                navigatorStyle: {
                    navBarBackgroundColor: Colors_1.default.Primary,
                    navBarTextColor: "#fff",
                    navBarTitleTextCentered: true,
                    topBarElevationShadowEnabled: false,
                    statusBarTextColorScheme: "light"
                }
            }, {
                label: "Saved Jobs",
                screen: ScreenNames_1.ScreenNames.Review,
                icon: await FontAwesome_1.default.getImageSource("bookmark-o", 22),
                selectedIcon: await FontAwesome_1.default.getImageSource("bookmark", 22),
                title: "Saved Jobs",
                navigatorStyle: {
                    navBarBackgroundColor: Colors_1.default.Primary,
                    navBarTextColor: "#fff",
                    navBarTitleTextCentered: true,
                    topBarElevationShadowEnabled: false,
                    navBarButtonColor: "#fff",
                    statusBarTextColorScheme: "light"
                }
            }
        ],
        tabsStyle: {
            tabBarSelectedButtonColor: Colors_1.default.Primary,
            tabBarButtonColor: "#333",
            tabBarLabelColor: "#333",
            tabBarSelectedLabelColor: Colors_1.default.Primary
        },
        appStyle: {
            tabBarButtonColor: "#333",
            tabBarSelectedButtonColor: Colors_1.default.Primary
        }
    });
}
exports.startAppWithMainScreens = startAppWithMainScreens;
//# sourceMappingURL=index.js.map