"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_react_1 = require("mobx-react");
const React = require("react");
const Slides_1 = require("../components/Slides");
const react_native_fbsdk_1 = require("react-native-fbsdk");
const StoreNames_1 = require("../constants/StoreNames");
const index_1 = require("./index");
const SLIDE_DATA = [
    { text: "Welcome to JobApp", backgroundColor: "#03A9F4" },
    { text: "Use this to get a job", backgroundColor: "#009688" },
    { text: "Set your location, then swipe away", backgroundColor: "#03A9F4" }
];
let WelcomeScreen = class WelcomeScreen extends React.Component {
    constructor() {
        super(...arguments);
        this.skipWelcome = async () => {
            setTimeout(async () => await index_1.startAppWithMainScreens(), 100);
        };
        this.loginFaceBook = async () => {
            if (this.authenticated) {
                await this.skipWelcome();
                return;
            }
            try {
                const result = await react_native_fbsdk_1.LoginManager.logInWithReadPermissions(["public_profile"]);
                if (result.isCancelled)
                    return;
                const accessToken = await react_native_fbsdk_1.AccessToken.getCurrentAccessToken();
                this.fbAuthStore.setAccessToken(accessToken.accessToken).then();
                await this.skipWelcome();
            }
            catch (err) {
                console.warn(err);
            }
        };
    }
    get authenticated() {
        return !!this.fbAuthStore.accessToken;
    }
    get fbAuthStore() {
        return this.props[StoreNames_1.default.FBAuthStore];
    }
    render() {
        return <Slides_1.default data={SLIDE_DATA} onComplete={this.loginFaceBook}/>;
    }
};
WelcomeScreen = __decorate([
    mobx_react_1.inject(StoreNames_1.default.FBAuthStore),
    mobx_react_1.observer
], WelcomeScreen);
exports.default = WelcomeScreen;
//# sourceMappingURL=WelcomeScreen.js.map