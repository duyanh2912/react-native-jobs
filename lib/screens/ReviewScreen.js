"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_react_1 = require("mobx-react");
const React = require("react");
const react_native_1 = require("react-native");
const StoreNames_1 = require("../constants/StoreNames");
const CompactJobCard_1 = require("../components/CompactJobCard");
const ScreenNames_1 = require("../constants/ScreenNames");
let ReviewScreen = class ReviewScreen extends React.Component {
    constructor(props) {
        super(props);
        this.onNavigatorEvent = (event) => {
            if (event.id === "setting") {
                this.props.navigator.push({
                    screen: ScreenNames_1.default.Setting,
                    animationType: "slide-horizontal",
                    title: "Setting"
                });
            }
            else if (event.id === "bottomTabReselected") {
                this.flatList.scrollToOffset({ offset: 0, animated: true });
            }
        };
        this.renderLikedJobs = (item) => {
            return <CompactJobCard_1.default job={item.item}/>;
        };
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }
    get jobStore() {
        return this.props[StoreNames_1.default.JobStore];
    }
    componentWillUpdate() {
        if (react_native_1.UIManager.setLayoutAnimationEnabledExperimental) {
            react_native_1.UIManager.setLayoutAnimationEnabledExperimental(true);
        }
        react_native_1.LayoutAnimation.easeInEaseOut();
    }
    render() {
        return (<react_native_1.FlatList data={this.jobStore.likedJobs.slice()} renderItem={this.renderLikedJobs} style={{ flex: 1, backgroundColor: "white" }} contentContainerStyle={{ paddingBottom: 15 }} keyExtractor={item => item.jobkey} ref={e => this.flatList = e}/>);
    }
};
ReviewScreen.navigatorButtons = {
    leftButtons: [
        {
            id: "sideMenu"
        }
    ],
    rightButtons: [
        {
            title: "Setting",
            id: "setting"
        }
    ]
};
ReviewScreen = __decorate([
    mobx_react_1.inject(StoreNames_1.default.JobStore),
    mobx_react_1.observer
], ReviewScreen);
exports.default = ReviewScreen;
//# sourceMappingURL=ReviewScreen.js.map