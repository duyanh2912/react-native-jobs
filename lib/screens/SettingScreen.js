"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_react_1 = require("mobx-react");
const React = require("react");
const react_native_elements_1 = require("react-native-elements");
const StoreNames_1 = require("../constants/StoreNames");
const react_native_1 = require("react-native");
let SettingScreen = class SettingScreen extends React.Component {
    constructor() {
        super(...arguments);
        this.resetButtonPressed = () => {
            this.jobStore.resetLikedJobs();
            this.props.navigator.pop();
        };
    }
    get jobStore() {
        return this.props[StoreNames_1.default.JobStore];
    }
    render() {
        return (<react_native_1.View style={{ flex: 1, backgroundColor: "white" }}>
                <react_native_elements_1.Button large={true} icon={{ name: "delete-forever" }} backgroundColor="#F44336" title="Reset Liked Jobs" onPress={this.resetButtonPressed} containerViewStyle={{ marginTop: 15 }}/>
            </react_native_1.View>);
    }
    ;
};
SettingScreen = __decorate([
    mobx_react_1.inject(StoreNames_1.default.JobStore),
    mobx_react_1.observer
], SettingScreen);
exports.default = SettingScreen;
//# sourceMappingURL=SettingScreen.js.map