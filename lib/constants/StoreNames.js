"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var StoreNames;
(function (StoreNames) {
    StoreNames["FBAuthStore"] = "FB_AUTH_STORE";
    StoreNames["JobStore"] = "JOB_STORE";
})(StoreNames || (StoreNames = {}));
exports.default = StoreNames;
//# sourceMappingURL=StoreNames.js.map