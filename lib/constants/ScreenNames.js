"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ScreenNames;
(function (ScreenNames) {
    ScreenNames["Welcome"] = "welcomeScreen";
    ScreenNames["Map"] = "mapScreen";
    ScreenNames["Deck"] = "deckScreen";
    ScreenNames["Review"] = "reviewScreen";
    ScreenNames["Setting"] = "settingScreen";
})(ScreenNames = exports.ScreenNames || (exports.ScreenNames = {}));
exports.default = ScreenNames;
//# sourceMappingURL=ScreenNames.js.map