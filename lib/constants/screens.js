"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LOGIN_SCREEN = "iCheckPayApp.loginScreen";
exports.MAIN_SCREEN = "iCheckPayApp.mainScreen";
exports.CREATE_TRANSFER_SCREEN = "iCheckPayApp.createTransferScreen";
exports.CREATE_PAYOUT_SCREEN = "iCheckPayApp.createPayoutScreen";
exports.CREATE_RECHARGE_SCREEN = "iCheckPayApp.createRechargeScreen";
var ScreenNames;
(function (ScreenNames) {
    ScreenNames["Welcome"] = "welcomeScreen";
})(ScreenNames = exports.ScreenNames || (exports.ScreenNames = {}));
//# sourceMappingURL=screens.js.map