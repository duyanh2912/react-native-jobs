"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const CodePush = require("react-native-code-push");
const CODE_PUSH_OPTIONS = {
    installMode: CodePush.InstallMode.ON_NEXT_RESUME,
    checkFrequency: CodePush.CheckFrequency.ON_APP_RESUME,
};
exports.CODE_PUSH_OPTIONS = CODE_PUSH_OPTIONS;
//# sourceMappingURL=Codepush.js.map