"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const reverseGeocode = require("latlng-to-zip");
const mobx_1 = require("mobx");
const qs = require("qs");
const mobx_persist_1 = require("mobx-persist");
const react_native_1 = require("react-native");
class JobStore {
    constructor() {
        this.likedJobs = [];
        this.likedJobIds = [];
        this.getJobsFromCoordinate = async (coordinate) => {
            const zip = await this.getZipCodeFromLatLng(coordinate);
            const url = this.getJobUrlFromZip(zip);
            const body = await fetch(url);
            const data = await body.json();
            this.jobs = data.results;
        };
        this.addJobToLiked = (job) => {
            if (this.likedJobIds.indexOf(job.jobkey) !== -1)
                return;
            this.likedJobIds.push(job.jobkey);
            this.likedJobs.push(job);
        };
        this.resetLikedJobs = () => {
            this.likedJobs = [];
            this.likedJobIds = [];
        };
        this.getZipCodeFromLatLng = async (coordinate) => {
            return await reverseGeocode(coordinate);
        };
        this.getJobUrlFromZip = (zip) => {
            const params = Object.assign({}, JobStore.JOB_QUERY_PARAMS, { l: zip });
            const query = qs.stringify(params);
            return `${JobStore.JOB_ROOT_URL}${query}`;
        };
    }
    static async default() {
        const hydrate = mobx_persist_1.create({ storage: react_native_1.AsyncStorage });
        const store = new JobStore();
        await hydrate("jobStore", store);
        return store;
    }
}
JobStore.JOB_ROOT_URL = "http://api.indeed.com/ads/apisearch?";
JobStore.PUBLISHER = "4201738803816157";
JobStore.JOB_QUERY_PARAMS = {
    publisher: JobStore.PUBLISHER,
    format: "json",
    v: "2",
    q: "javascript",
    latlong: 1,
    radius: 10,
    limit: 10
};
__decorate([
    mobx_1.observable
], JobStore.prototype, "jobs", void 0);
__decorate([
    mobx_persist_1.persist("list"), mobx_1.observable
], JobStore.prototype, "likedJobs", void 0);
__decorate([
    mobx_persist_1.persist("list"), mobx_1.observable
], JobStore.prototype, "likedJobIds", void 0);
__decorate([
    mobx_1.action
], JobStore.prototype, "getJobsFromCoordinate", void 0);
__decorate([
    mobx_1.action
], JobStore.prototype, "addJobToLiked", void 0);
__decorate([
    mobx_1.action
], JobStore.prototype, "resetLikedJobs", void 0);
__decorate([
    mobx_1.action
], JobStore.prototype, "getZipCodeFromLatLng", void 0);
__decorate([
    mobx_1.action
], JobStore.prototype, "getJobUrlFromZip", void 0);
exports.default = JobStore;
//# sourceMappingURL=JobStore.js.map