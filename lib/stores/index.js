"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const StoreNames_1 = require("../constants/StoreNames");
const FBAuthStore_1 = require("./FBAuthStore");
const JobStore_1 = require("./JobStore");
exports.getStores = async () => ({
    [StoreNames_1.default.FBAuthStore]: await FBAuthStore_1.default.default(),
    [StoreNames_1.default.JobStore]: await JobStore_1.default.default()
});
//# sourceMappingURL=index.js.map