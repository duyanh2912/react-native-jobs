"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_1 = require("mobx");
const react_native_1 = require("react-native");
class FBAuthStore {
    constructor() {
        this.loadLocalAccessToken = async () => {
            this.accessToken = await react_native_1.AsyncStorage.getItem("fb_token");
        };
        this.setAccessToken = async (token) => {
            await react_native_1.AsyncStorage.setItem("fb_token", token);
            this.accessToken = token;
        };
        this.resetToken = async () => {
            await react_native_1.AsyncStorage.removeItem("fb_token");
            this.accessToken = undefined;
        };
    }
    static async default() {
        const store = new FBAuthStore();
        await store.loadLocalAccessToken();
        return store;
    }
}
__decorate([
    mobx_1.observable
], FBAuthStore.prototype, "accessToken", void 0);
__decorate([
    mobx_1.action
], FBAuthStore.prototype, "loadLocalAccessToken", void 0);
__decorate([
    mobx_1.action
], FBAuthStore.prototype, "setAccessToken", void 0);
__decorate([
    mobx_1.action
], FBAuthStore.prototype, "resetToken", void 0);
exports.default = FBAuthStore;
//# sourceMappingURL=FBAuthStore.js.map