"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_react_1 = require("mobx-react");
const React = require("react");
const react_native_elements_1 = require("react-native-elements");
const react_native_1 = require("react-native");
const MapView = require("react-native-maps");
const mobx_1 = require("mobx");
const Colors_1 = require("../constants/Colors");
let CompactJobCard = class CompactJobCard extends React.Component {
    get region() {
        return {
            latitude: this.props.job.latitude,
            longitude: this.props.job.longitude,
            latitudeDelta: 0.0045,
            longitudeDelta: 0.002
        };
    }
    render() {
        const job = this.props.job;
        return (<react_native_elements_1.Card title={job.jobtitle}>
                <react_native_1.View style={{ height: 220 }}>
                    <MapView cacheEnabled={true} scrollEnabled={false} zoomEnabled={false} rotateEnabled={false} pitchEnabled={false} style={{ flex: 1 }} provider="google" region={this.region}>
                        <MapView.Marker coordinate={this.region}/>
                    </MapView>
                    <react_native_1.View style={styles.detailWrapper}>
                        <react_native_1.Text style={styles.italics}>{job.company}</react_native_1.Text>
                        <react_native_1.Text style={styles.italics}>{job.formattedRelativeTime}</react_native_1.Text>
                    </react_native_1.View>
                    <react_native_elements_1.Button title="Apply Now!" onPress={() => react_native_1.Linking.openURL(job.url)} backgroundColor={Colors_1.default.Primary}/>
                </react_native_1.View>
            </react_native_elements_1.Card>);
    }
};
__decorate([
    mobx_1.computed
], CompactJobCard.prototype, "region", null);
CompactJobCard = __decorate([
    mobx_react_1.observer
], CompactJobCard);
const styles = react_native_1.StyleSheet.create({
    detailWrapper: {
        flexDirection: "row",
        marginVertical: 10,
        justifyContent: "space-around"
    },
    italics: {
        fontStyle: "italic"
    }
});
exports.default = CompactJobCard;
//# sourceMappingURL=CompactJobCard.js.map