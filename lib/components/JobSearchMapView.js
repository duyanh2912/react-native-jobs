"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_react_1 = require("mobx-react");
const React = require("react");
const MapView = require("react-native-maps");
let JobSearchMapView = class JobSearchMapView extends React.Component {
    constructor(props) {
        super(props);
        this.onRegionChangeComplete = (region) => {
            this.setState({ region });
            this.props.onRegionChangeComplete(region);
        };
        this.renderMarkers = () => {
            if (!this.props.jobs)
                return null;
            return this.props.jobs.map(job => {
                return (<MapView.Marker coordinate={{
                    latitude: job.latitude,
                    longitude: job.longitude
                }} key={job.jobkey}/>);
            });
        };
        this.state = { region: props.initialRegion };
    }
    render() {
        return (<MapView style={this.props.style} region={this.state.region} initialRegion={this.state.region} onRegionChangeComplete={this.onRegionChangeComplete} provider="google">
                {this.renderMarkers()}
            </MapView>);
    }
    shouldComponentUpdate(newProps) {
        return this.props.initialRegion !== newProps.initialRegion || this.props.jobs !== newProps.jobs;
    }
};
JobSearchMapView = __decorate([
    mobx_react_1.observer
], JobSearchMapView);
exports.default = JobSearchMapView;
//# sourceMappingURL=JobSearchMapView.js.map