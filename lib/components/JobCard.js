"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_react_1 = require("mobx-react");
const react_native_elements_1 = require("react-native-elements");
const React = require("react");
const react_native_1 = require("react-native");
const mobx_1 = require("mobx");
const MapView = require("react-native-maps");
let JobCard = class JobCard extends React.Component {
    get region() {
        return {
            latitude: this.props.job.latitude,
            longitude: this.props.job.longitude,
            latitudeDelta: 0.0045,
            longitudeDelta: 0.002
        };
    }
    render() {
        const job = this.props.job;
        return (<react_native_elements_1.Card title={job.jobtitle} titleStyle={styles.title} titleNumberOfLines={1}>
                <MapView scrollEnabled={false} zoomEnabled={false} rotateEnabled={false} pitchEnabled={false} style={{ height: 300 }} provider="google" cacheEnabled={true} region={this.region} initialRegion={this.region}>
                    <MapView.Marker coordinate={this.region}/>
                </MapView>
                <react_native_1.View style={styles.detailWrapper}>
                    <react_native_elements_1.Text style={styles.detailText}>{job.company}</react_native_elements_1.Text>
                    <react_native_elements_1.Text style={styles.detailText}>{job.formattedRelativeTime}</react_native_elements_1.Text>
                </react_native_1.View>
                <react_native_elements_1.Text style={styles.jobSnippet}>
                    {job.snippet.replace(/(<b>|<\/b>)/g, "")}
                </react_native_elements_1.Text>
            </react_native_elements_1.Card>);
    }
};
__decorate([
    mobx_1.computed
], JobCard.prototype, "region", null);
JobCard = __decorate([
    mobx_react_1.observer
], JobCard);
const styles = react_native_1.StyleSheet.create({
    title: {
        height: 21.3
    },
    detailWrapper: {
        flexDirection: "row",
        justifyContent: "space-around",
        marginBottom: 10,
        marginTop: 10
    },
    detailText: {
        fontWeight: "bold"
    },
    jobSnippet: {
        height: 67
    }
});
exports.default = JobCard;
//# sourceMappingURL=JobCard.js.map