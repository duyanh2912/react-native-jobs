"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_react_1 = require("mobx-react");
const React = require("react");
const react_native_1 = require("react-native");
const react_native_elements_1 = require("react-native-elements");
let SlidePage = class SlidePage extends React.Component {
    renderButton() {
        if (!this.props.last)
            return null;
        return (<react_native_elements_1.Button title="Onwards!" onPress={this.props.onComplete} raised={true} buttonStyle={styles.button} containerViewStyle={styles.buttonContainer}/>);
    }
    render() {
        const slide = this.props.slide;
        return (<react_native_1.View style={[styles.container, { backgroundColor: slide.backgroundColor }]}>
                <react_native_1.Text style={styles.text}>{slide.text}</react_native_1.Text>
                {this.renderButton()}
            </react_native_1.View>);
    }
};
SlidePage = __decorate([
    mobx_react_1.observer
], SlidePage);
const styles = react_native_1.StyleSheet.create({
    text: {
        fontSize: 30,
        textAlign: "center",
        color: "white",
        fontWeight: "bold"
    },
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        width: react_native_1.Dimensions.get("window").width,
        paddingHorizontal: 30,
        paddingBottom: 50
    },
    button: {
        backgroundColor: "#0288D1"
    },
    buttonContainer: {
        marginTop: 20
    }
});
exports.default = SlidePage;
//# sourceMappingURL=SlidePage.js.map