"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_1 = require("react-native");
const React = require("react");
var SwipeDirection;
(function (SwipeDirection) {
    SwipeDirection["Left"] = "left";
    SwipeDirection["Right"] = "right";
})(SwipeDirection || (SwipeDirection = {}));
const SCREEN_WIDTH = react_native_1.Dimensions.get("window").width;
const SWIPE_THRESHOLD = SCREEN_WIDTH * 0.4;
const SWIPE_SPEED = 1;
const USE_NATIVE_DRIVER = true;
class Deck extends React.Component {
    constructor(props) {
        super(props);
        this.forceSwipe = (currentX) => {
            const toValue = currentX > 0 ? SCREEN_WIDTH : -SCREEN_WIDTH;
            const duration = (SCREEN_WIDTH + (currentX > 0 ? -currentX : currentX)) / SWIPE_SPEED;
            react_native_1.Animated.timing(this.position.x, {
                toValue,
                duration,
                useNativeDriver: USE_NATIVE_DRIVER
            }).start(() => this.onSwipeComplete(currentX > 0 ? SwipeDirection.Right : SwipeDirection.Left));
        };
        this.onSwipeComplete = (direction) => {
            const { onSwipeLeft, onSwipeRight } = this.props;
            const item = this.props.data[this.state.selectedIndex];
            direction === SwipeDirection.Left ? onSwipeLeft(item) : onSwipeRight(item);
            this.position.setValue({ x: 0, y: 0 });
            this.setState({ selectedIndex: this.state.selectedIndex + 1 });
        };
        this.resetPosition = () => {
            react_native_1.Animated.spring(this.position.x, {
                toValue: 0,
                useNativeDriver: USE_NATIVE_DRIVER
            }).start();
        };
        this.getTopCardStyle = () => {
            const position = this.position;
            const rotate = position.x.interpolate({
                inputRange: [-SCREEN_WIDTH * 1.5, 0, SCREEN_WIDTH * 1.5],
                outputRange: ["-120deg", "0deg", "120deg"]
            });
            const translateX = position.x.interpolate({
                inputRange: [-1, 1],
                outputRange: [-1, 1]
            });
            return {
                // ...position.getLayout(),
                transform: [
                    { translateX },
                    { rotate },
                    { perspective: 1000 }
                ]
            };
        };
        this.renderCards = () => {
            if (!this.props.data || this.state.selectedIndex >= this.props.data.length) {
                return this.props.renderNoMoreCard();
            }
            const cards = this.props.data.map((item, index) => {
                if (index < this.state.selectedIndex) {
                    return (<react_native_1.Animated.View key={this.props.keyExtractor(item)} style={{ transform: [{ translateX: SCREEN_WIDTH }] }}/>);
                }
                if (index === this.state.selectedIndex) {
                    return (<react_native_1.Animated.View key={this.props.keyExtractor(item)} style={[this.getTopCardStyle(), styles.card, { zIndex: 10 }]} {...this.panResponder.panHandlers}>
                        {this.props.renderCard(item)}
                    </react_native_1.Animated.View>);
                }
                return (<react_native_1.Animated.View key={this.props.keyExtractor(item)} style={[styles.card, {
                        top: 10 * (index - this.state.selectedIndex),
                        zIndex: -index
                    }]}>
                    {this.props.renderCard(item)}
                </react_native_1.Animated.View>);
            });
            return cards.reverse();
        };
        this.position = new react_native_1.Animated.ValueXY();
        this.panResponder = react_native_1.PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: (event, gesture) => {
                this.position.setValue({ x: gesture.dx, y: 0 });
            },
            onPanResponderRelease: (event, gesture) => {
                if (gesture.dx > SWIPE_THRESHOLD) {
                    this.forceSwipe(gesture.dx);
                }
                else if (gesture.dx < -SWIPE_THRESHOLD) {
                    this.forceSwipe(gesture.dx);
                }
                else {
                    this.resetPosition();
                }
            }
        });
        this.state = { selectedIndex: 0 };
    }
    componentWillUpdate(nextProps, nextState) {
        if (!this.props.data
            || !nextProps.data
            || nextState.selectedIndex >= nextProps.data.length
            || this.state.selectedIndex >= nextState.selectedIndex)
            return;
        if (react_native_1.UIManager.setLayoutAnimationEnabledExperimental) {
            react_native_1.UIManager.setLayoutAnimationEnabledExperimental(true);
        }
        react_native_1.LayoutAnimation.spring();
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.data !== this.props.data) {
            this.setState({ selectedIndex: 0 });
        }
    }
    render() {
        return (<react_native_1.View style={this.props.containerStyle}>
                {this.renderCards()}
            </react_native_1.View>);
    }
}
Deck.defaultProps = {
    onSwipeLeft: () => {
    },
    onSwipeRight: () => {
    }
};
const styles = react_native_1.StyleSheet.create({
    card: {
        position: "absolute",
        width: SCREEN_WIDTH
    }
});
exports.default = Deck;
//# sourceMappingURL=Deck.js.map