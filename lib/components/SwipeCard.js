"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_native_elements_1 = require("react-native-elements");
const react_native_1 = require("react-native");
const React = require("react");
class SwipeCard extends React.Component {
    shouldComponentUpdate(newProps) {
        return newProps.item.id !== this.props.item.id;
    }
    render() {
        const item = this.props.item;
        return (<react_native_elements_1.Card image={{ uri: item.uri }}>
                <react_native_1.Text style={styles.title}>{item.text}</react_native_1.Text>
                <react_native_1.Text style={styles.subTitle}>I can customize the card further</react_native_1.Text>
                <react_native_elements_1.Button title="View Now!" icon={{ name: "code" }} backgroundColor="#03A9f4" onPress={() => {
        }} containerViewStyle={styles.buttonContainer}/>
            </react_native_elements_1.Card>);
    }
}
const styles = react_native_1.StyleSheet.create({
    title: {
        fontSize: 18,
        fontWeight: "600",
        marginBottom: 5
    },
    subTitle: {
        marginBottom: 8
    },
    buttonContainer: {
        alignSelf: "stretch",
        marginLeft: 0,
        marginRight: 0
    }
});
exports.default = SwipeCard;
//# sourceMappingURL=SwipeCard.js.map