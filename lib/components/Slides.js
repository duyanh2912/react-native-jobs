"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mobx_react_1 = require("mobx-react");
const React = require("react");
const react_native_1 = require("react-native");
const SlidePage_1 = require("./SlidePage");
let Slides = class Slides extends React.Component {
    renderSlides() {
        return this.props.data.map((slide, index) => {
            return (<SlidePage_1.default slide={slide} key={index} onComplete={this.props.onComplete} last={index === this.props.data.length - 1}/>);
        });
    }
    render() {
        return (<react_native_1.ScrollView horizontal={true} pagingEnabled={true} showsHorizontalScrollIndicator={false} bounces={false}>
                {this.renderSlides()}
            </react_native_1.ScrollView>);
    }
};
Slides = __decorate([
    mobx_react_1.observer
], Slides);
exports.default = Slides;
//# sourceMappingURL=Slides.js.map