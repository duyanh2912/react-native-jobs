package com.jobs;

import android.app.Application;
import android.content.Intent;

import com.facebook.react.ReactApplication;

// Code push
import com.microsoft.codepush.react.ReactInstanceHolder;
import com.microsoft.codepush.react.CodePush;

import com.facebook.reactnative.androidsdk.FBSDKPackage;

import com.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

// react-native-navigation
import com.reactnativenavigation.NavigationApplication;

// FB SDK
import com.facebook.FacebookSdk;
import com.facebook.CallbackManager;
import com.facebook.appevents.AppEventsLogger;

// react-native-maps
import com.airbnb.android.react.maps.MapsPackage;
import com.reactnativenavigation.controllers.ActivityCallbacks;


public class MainApplication extends NavigationApplication {
  private static CallbackManager mCallbackManager = CallbackManager.Factory.create();
  protected static CallbackManager getCallbackManager() {
    return mCallbackManager;
  }

  @Override
  public boolean isDebug() {
    // Make sure you are using BuildConfig from your own application
    return BuildConfig.DEBUG;
  }

  @Override
  public String getJSBundleFile() {
    return CodePush.getJSBundleFile();
  }

  protected List<ReactPackage> getPackages() {
    // Add additional packages you require here
    // No need to add RnnPackage and MainReactPackage
    return Arrays.<ReactPackage>asList(
            // eg. new VectorIconsPackage()
            new VectorIconsPackage(),
            new FBSDKPackage(mCallbackManager),
            new MapsPackage(),
            new CodePush("20ywvy3nSfuS3eDGhusbmxuY99oVc8622786-141e-43ea-b0c9-cfce35f23f48", this, BuildConfig.DEBUG)
    );
  }

  @Override
  public List<ReactPackage> createAdditionalReactPackages() {
    return getPackages();
  }

  @Override
  public void onCreate() {
    super.onCreate();

    // add this
    setActivityCallbacks(new ActivityCallbacks() {
      @Override
      public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
      }
    });

    FacebookSdk.sdkInitialize(getApplicationContext());

    // If you want to use AppEventsLogger to log events.
    AppEventsLogger.activateApp(this);
  }
}
