import {registerScreens, startAppWithMainScreens} from "./screens";
import {getStores} from "./stores";
import Provider from "./utils/MobxRnnProvider"
import * as React from "react";

class App extends React.Component {
    static start = () => {
        getStores().then(async (stores) => {
            registerScreens(stores, Provider);
            if (!stores.FB_AUTH_STORE.accessToken) {
                // startAppWithWelcomeScreen();
                await startAppWithMainScreens();
            } else {
                await startAppWithMainScreens();
            }
        });
    }
}

App.start();