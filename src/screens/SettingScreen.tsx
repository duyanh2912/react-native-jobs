import {inject, observer} from "mobx-react";
import * as React from "react";
import {Button} from "react-native-elements";
import StoreNames from "../constants/StoreNames";
import JobStore from "../stores/JobStore";
import {ReactNativeNavigationProps} from "../utils/ReactNativeNavigationProps";
import {View} from "react-native";

@inject(StoreNames.JobStore)
@observer
class SettingScreen extends React.Component<ReactNativeNavigationProps> {
    get jobStore() {
        return this.props[StoreNames.JobStore] as JobStore
    }

    resetButtonPressed = () => {
        this.jobStore.resetLikedJobs();
        this.props.navigator.pop();
    };

    render() {
        return (
            <View style={{flex: 1, backgroundColor: "white"}}>
                <Button
                    large={true}
                    icon={{name: "delete-forever"}}
                    backgroundColor="#F44336"
                    title="Reset Liked Jobs"
                    onPress={this.resetButtonPressed}
                    containerViewStyle={{marginTop: 15}}
                />
            </View>
        )
    };
}

export default SettingScreen;