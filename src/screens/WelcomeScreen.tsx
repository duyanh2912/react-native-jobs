import {inject, observer} from "mobx-react";
import * as React from "react";
import Slides from "../components/Slides";
import {ReactNativeNavigationProps} from "../utils/ReactNativeNavigationProps";
import {AccessToken, LoginManager} from "react-native-fbsdk";
import StoreNames from "../constants/StoreNames";
import FBAuthStore from "../stores/FBAuthStore";
import {startAppWithMainScreens} from "./index";

const SLIDE_DATA: SlideDataModel[] = [
    {text: "Welcome to JobApp", backgroundColor: "#03A9F4"},
    {text: "Use this to get a job", backgroundColor: "#009688"},
    {text: "Set your location, then swipe away", backgroundColor: "#03A9F4"}
];

@inject(StoreNames.FBAuthStore)
@observer
class WelcomeScreen extends React.Component<ReactNativeNavigationProps> {
    get authenticated(): boolean {
        return !!this.fbAuthStore.accessToken
    }

    get fbAuthStore() {
        return this.props[StoreNames.FBAuthStore] as FBAuthStore
    }

    skipWelcome = async () => {
        setTimeout(async () => await startAppWithMainScreens(), 100)
    };

    loginFaceBook = async () => {
        if (this.authenticated) {
            await this.skipWelcome();
            return
        }

        try {
            const result = await LoginManager.logInWithReadPermissions(["public_profile"]);
            if (result.isCancelled) return;

            const accessToken = await AccessToken.getCurrentAccessToken();
            this.fbAuthStore.setAccessToken(accessToken!.accessToken).then();
            await this.skipWelcome();
        } catch (err) {
            console.warn(err);
        }
    };

    render() {
        return <Slides data={SLIDE_DATA} onComplete={this.loginFaceBook}/>
    }
}

export default WelcomeScreen