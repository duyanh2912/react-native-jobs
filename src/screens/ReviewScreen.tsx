import {inject, observer} from "mobx-react";
import * as React from "react";
import {FlatList, LayoutAnimation, ListRenderItemInfo, UIManager} from "react-native";
import StoreNames from "../constants/StoreNames";
import JobStore from "../stores/JobStore";
import IndeedJobResultModel from "../models/IndeedJobResultModel";
import CompactJobCard from "../components/CompactJobCard";
import {ReactNativeNavigationProps} from "../utils/ReactNativeNavigationProps";
import ScreenNames from "../constants/ScreenNames";

@inject(StoreNames.JobStore)
@observer
class ReviewScreen extends React.Component<ReactNativeNavigationProps> {
    static navigatorButtons = {
        leftButtons: [
            {
                id: "sideMenu"
            }
        ],
        rightButtons: [
            {
                title: "Setting",
                id: "setting"
            }
        ]
    };

    flatList: FlatList<any>;

    get jobStore() {
        return this.props[StoreNames.JobStore] as JobStore
    }

    constructor(props: any) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }

    componentWillUpdate() {
        if (UIManager.setLayoutAnimationEnabledExperimental) {
            UIManager.setLayoutAnimationEnabledExperimental(true)
        }
        LayoutAnimation.easeInEaseOut()
    }

    onNavigatorEvent = (event: any) => {
        if (event.id === "setting") {
            this.props.navigator.push({
                screen: ScreenNames.Setting,
                animationType: "slide-horizontal",
                title: "Setting"
            })
        } else if (event.id === "bottomTabReselected") {
            this.flatList.scrollToOffset({offset: 0, animated: true})
        }
    };

    renderLikedJobs = (item: ListRenderItemInfo<IndeedJobResultModel>) => {
        return <CompactJobCard job={item.item}/>
    };

    render() {
        return (
            <FlatList
                data={this.jobStore.likedJobs.slice()}
                renderItem={this.renderLikedJobs}
                style={{flex: 1, backgroundColor: "white"}}
                contentContainerStyle={{paddingBottom: 15}}
                keyExtractor={item => item.jobkey}
                ref={e => this.flatList = e as any}
            />
        )
    }
}

export default ReviewScreen;