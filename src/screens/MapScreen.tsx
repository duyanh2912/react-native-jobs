import {inject, observer} from "mobx-react";
import * as React from "react";
import {observable} from "mobx";
import {ActivityIndicator, StyleSheet, View} from "react-native";
import CoordinateModel from "../models/CoordinateModel";
import {Button} from "react-native-elements";
import StoreNames from "../constants/StoreNames";
import JobStore from "../stores/JobStore";
import {ReactNativeNavigationProps} from "../utils/ReactNativeNavigationProps";
import JobSearchMapView from "../components/JobSearchMapView";
import Colors from "../constants/Colors";
import {CODE_PUSH_OPTIONS} from "../constants/Codepush";

const codePush = require("react-native-code-push");

@codePush(CODE_PUSH_OPTIONS)
@inject(StoreNames.JobStore)
@observer
class MapScreen extends React.Component<ReactNativeNavigationProps> {
    @observable isMapLoaded = false;
    @observable isFetchingJobs = false;
    @observable initialRegion: CoordinateModel = {
        longitude: -122,
        latitude: 37,
        longitudeDelta: 0.04,
        latitudeDelta: 0.09,
    };
    @observable currentRegion: CoordinateModel;

    get jobStore() {
        return this.props[StoreNames.JobStore] as JobStore
    }

    componentDidMount() {
        this.isMapLoaded = true;
    }

    onRegionChangeComplete = (region: any) => {
        this.currentRegion = region;
    };

    onButtonPress = async () => {
        try {
            this.isFetchingJobs = true;
            await this.jobStore.getJobsFromCoordinate(this.currentRegion);
            this.props.navigator.switchToTab({tabIndex: 1});
        } catch (err) {
            alert(err.message)
        } finally {
            this.isFetchingJobs = false;
        }
    };

    render() {
        if (!this.isMapLoaded) {
            return (
                <View style={{flex: 1, justifyContent: "center"}}>
                    <ActivityIndicator size="large"/>
                </View>
            )
        }

        return (
            <View style={{flex: 1}}>
                <JobSearchMapView
                    style={{flex: 1}}
                    initialRegion={this.initialRegion}
                    onRegionChangeComplete={this.onRegionChangeComplete}
                    jobs={this.jobStore.jobs}
                />
                <Button
                    large={true}
                    title="Search this area"
                    backgroundColor={Colors.Primary}
                    icon={{name: "search"}}
                    onPress={this.onButtonPress}
                    containerViewStyle={styles.buttonContainer}
                    disabled={this.isFetchingJobs}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    buttonContainer: {
        position: "absolute",
        bottom: 20,
        left: 16,
        right: 16
    }
});

export default MapScreen;