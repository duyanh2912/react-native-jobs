import WelcomeScreen from "./WelcomeScreen";
import MapScreen from "./MapScreen";
import {Navigation} from "react-native-navigation";
import {ScreenNames} from "../constants/ScreenNames";
import Icon from "react-native-vector-icons/dist/FontAwesome"
import DeckScreen from "./DeckScreen";
import ReviewScreen from "./ReviewScreen";
import SettingScreen from "./SettingScreen";
import Colors from "../constants/Colors";

const screens: { [id: string]: Element } = {
    [ScreenNames.Welcome]: WelcomeScreen,
    [ScreenNames.Map]: MapScreen,
    [ScreenNames.Deck]: DeckScreen,
    [ScreenNames.Review]: ReviewScreen,
    [ScreenNames.Setting]: SettingScreen
};

export function registerScreens(store: {}, Provider: {}) {
    Object.keys(screens).forEach(key => {
        Navigation.registerComponent(key, () => screens[key], store, Provider);
    });
}

export function startAppWithWelcomeScreen() {
    Navigation.startSingleScreenApp({
        screen: {
            screen: ScreenNames.Welcome,
            navigatorStyle: {
                navBarHidden: true
            }
        }
    });
}

export async function startAppWithMainScreens() {
    Navigation.startTabBasedApp({
        tabs: [
            {
                label: "Location",
                screen: ScreenNames.Map,
                navigatorStyle: {
                    navBarHidden: true
                },
                icon: await Icon.getImageSource("map-o", 22),
                selectedIcon: await Icon.getImageSource("map", 22)
            }, {
                label: "Rate",
                screen: ScreenNames.Deck,
                icon: await Icon.getImageSource("star-o", 22),
                selectedIcon: await Icon.getImageSource("star", 22),
                title: "Rate",
                navigatorStyle: {
                    navBarBackgroundColor: Colors.Primary,
                    navBarTextColor: "#fff",
                    navBarTitleTextCentered: true,
                    topBarElevationShadowEnabled: false,
                    statusBarTextColorScheme: "light"
                }
            }, {
                label: "Saved Jobs",
                screen: ScreenNames.Review,
                icon: await Icon.getImageSource("bookmark-o",22),
                selectedIcon: await Icon.getImageSource("bookmark", 22),
                title: "Saved Jobs",
                navigatorStyle: {
                    navBarBackgroundColor: Colors.Primary,
                    navBarTextColor: "#fff",
                    navBarTitleTextCentered: true,
                    topBarElevationShadowEnabled: false,
                    navBarButtonColor: "#fff",
                    statusBarTextColorScheme: "light"
                }
            }
        ],
        tabsStyle: {
            tabBarSelectedButtonColor: Colors.Primary,
            tabBarButtonColor: "#333",
            tabBarLabelColor: "#333",
            tabBarSelectedLabelColor: Colors.Primary
        },
        appStyle: {
            tabBarButtonColor: "#333",
            tabBarSelectedButtonColor: Colors.Primary
        }
    })
}