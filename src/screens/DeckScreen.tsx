import {inject, observer} from "mobx-react";
import {ReactNativeNavigationProps} from "../utils/ReactNativeNavigationProps";
import * as React from "react";
import StoreNames from "../constants/StoreNames";
import JobStore from "../stores/JobStore";
import Deck from "../components/Deck";
import IndeedJobResultModel from "../models/IndeedJobResultModel";
import JobCard from "../components/JobCard";
import {Button, Card} from "react-native-elements";
import Colors from "../constants/Colors";

@inject(StoreNames.JobStore)
@observer
class DeckScreen extends React.Component<ReactNativeNavigationProps> {
    get jobStore() {
        return this.props[StoreNames.JobStore] as JobStore
    }

    renderNoMoreCard = () => {
        return (
            <Card title="No more jobs">
                <Button
                    large={true}
                    title="Back To Map"
                    icon={{name: "my-location"}}
                    backgroundColor={Colors.Primary}
                    onPress={() => this.props.navigator.switchToTab({tabIndex: 0})}
                />
            </Card>
        )
    }

    renderJobCards = (job: IndeedJobResultModel) => {
        return <JobCard job={job}/>
    };

    addJobToLiked = (job: IndeedJobResultModel) => {
        // Push action to the end of the queue so the animation can run first
        setTimeout(() => this.jobStore.addJobToLiked(job), 1);
    };

    render() {
        return (
            <Deck
                data={this.jobStore.jobs}
                keyExtractor={(job: IndeedJobResultModel) => job.jobkey}
                renderCard={this.renderJobCards}
                renderNoMoreCard={this.renderNoMoreCard}
                onSwipeRight={(job: IndeedJobResultModel) => this.addJobToLiked(job)}
                containerStyle={{flex: 1, backgroundColor: "white"}}
            />
        )
    }
}

export default DeckScreen;