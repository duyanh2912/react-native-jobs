import {action, observable} from "mobx";
import {AsyncStorage} from "react-native";

class FBAuthStore {
    @observable accessToken?: string;

    static async default() {
        const store = new FBAuthStore();
        await store.loadLocalAccessToken();
        return store
    }

    @action loadLocalAccessToken = async () => {
        this.accessToken = await AsyncStorage.getItem("fb_token")
    };

    @action setAccessToken = async (token: string) => {
        await AsyncStorage.setItem("fb_token", token);
        this.accessToken = token;
    };

    @action resetToken = async () => {
        await AsyncStorage.removeItem("fb_token");
        this.accessToken = undefined;
    };
}

export default FBAuthStore;