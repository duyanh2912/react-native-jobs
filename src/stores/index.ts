import StoreNames from "../constants/StoreNames";
import FBAuthStore from "./FBAuthStore";
import JobStore from "./JobStore";

export const getStores = async () => ({
    [StoreNames.FBAuthStore]: await FBAuthStore.default(),
    [StoreNames.JobStore]: await JobStore.default()
});