import reverseGeocode = require("latlng-to-zip");
import {action, observable} from "mobx";
import * as qs from "qs";
import CoordinateModel from "../models/CoordinateModel";
import IndeedJobResultModel from "../models/IndeedJobResultModel";
import {create, persist} from "mobx-persist";
import {AsyncStorage} from "react-native";

class JobStore {
    private static JOB_ROOT_URL = "http://api.indeed.com/ads/apisearch?";
    private static PUBLISHER = "4201738803816157";
    private static JOB_QUERY_PARAMS = {
        publisher: JobStore.PUBLISHER,
        format: "json",
        v: "2",
        q: "javascript",
        latlong: 1,
        radius: 10,
        limit: 10
    };

    @observable jobs: IndeedJobResultModel[];
    @persist("list") @observable likedJobs: IndeedJobResultModel[] = [];
    @persist("list") @observable likedJobIds: string[] = [];

    static async default() {
        const hydrate = create({storage: AsyncStorage});
        const store = new JobStore();
        await hydrate("jobStore", store);
        return store;
    }

    @action getJobsFromCoordinate = async (coordinate: CoordinateModel) => {
        const zip = await this.getZipCodeFromLatLng(coordinate);
        const url = this.getJobUrlFromZip(zip);
        const body = await fetch(url);
        const data = await body.json();
        this.jobs = data.results;
    };

    @action addJobToLiked = (job: IndeedJobResultModel) => {
        if (this.likedJobIds.indexOf(job.jobkey) !== -1) return;
        this.likedJobIds.push(job.jobkey);
        this.likedJobs.push(job);
    };

    @action resetLikedJobs = () => {
        this.likedJobs = [];
        this.likedJobIds = [];
    }

    @action private getZipCodeFromLatLng = async (coordinate: CoordinateModel) => {
        return await reverseGeocode(coordinate);
    };

    @action private getJobUrlFromZip = (zip: string) => {
        const params = {...JobStore.JOB_QUERY_PARAMS, l: zip};
        const query = qs.stringify(params);
        return `${JobStore.JOB_ROOT_URL}${query}`;
    };
}

export default JobStore;