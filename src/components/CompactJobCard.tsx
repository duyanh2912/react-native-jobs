import {observer} from "mobx-react";
import * as React from "react";
import {Button, Card} from "react-native-elements";
import {Text, View, StyleSheet, Linking} from "react-native";
import IndeedJobResultModel from "../models/IndeedJobResultModel";
import MapView = require("react-native-maps");
import {computed} from "mobx";
import CoordinateModel from "../models/CoordinateModel";
import Colors from "../constants/Colors";

interface CompactJobCardProps {
    job: IndeedJobResultModel;
}

@observer class CompactJobCard extends React.Component<CompactJobCardProps> {
    @computed get region(): CoordinateModel {
        return {
            latitude: this.props.job.latitude,
            longitude: this.props.job.longitude,
            latitudeDelta: 0.0045,
            longitudeDelta: 0.002
        }
    }

    render() {
        const job = this.props.job;

        return (
            <Card title={job.jobtitle}>
                <View style={{height: 220}}>
                    <MapView
                        cacheEnabled={true}
                        scrollEnabled={false}
                        zoomEnabled={false}
                        rotateEnabled={false}
                        pitchEnabled={false}
                        style={{flex: 1}}
                        provider="google"
                        region={this.region}
                    >
                        <MapView.Marker coordinate={this.region}/>
                    </MapView>
                    <View style={styles.detailWrapper}>
                        <Text style={styles.italics}>{job.company}</Text>
                        <Text style={styles.italics}>{job.formattedRelativeTime}</Text>
                    </View>
                    <Button
                        title="Apply Now!"
                        onPress={() => Linking.openURL(job.url)}
                        backgroundColor={Colors.Primary}
                    />
                </View>
            </Card>
        )
    }
}

const styles = StyleSheet.create({
    detailWrapper: {
        flexDirection: "row",
        marginVertical: 10,
        justifyContent: "space-around"
    },
    italics: {
        fontStyle: "italic"
    }
});

export default  CompactJobCard;