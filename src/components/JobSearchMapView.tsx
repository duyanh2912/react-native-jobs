import {observer} from "mobx-react";
import * as React from "react";
import CoordinateModel from "../models/CoordinateModel";
import {ViewStyle} from "react-native";
import IndeedJobResultModel from "../models/IndeedJobResultModel";
import MapView = require("react-native-maps");

interface JobSearchMapViewProps {
    initialRegion: CoordinateModel;
    style: ViewStyle;
    jobs?: IndeedJobResultModel[];

    onRegionChangeComplete(coordinate: CoordinateModel): void;
}

interface JobSearchMapViewState {
    region: CoordinateModel
}

@observer
class JobSearchMapView extends React.Component<JobSearchMapViewProps, JobSearchMapViewState> {
    constructor(props: JobSearchMapViewProps) {
        super(props);
        this.state = {region: props.initialRegion}
    }

    onRegionChangeComplete = (region: CoordinateModel) => {
        this.setState({region});
        this.props.onRegionChangeComplete(region);
    };

    renderMarkers = () => {
        if (!this.props.jobs) return null;
        return this.props.jobs.map(job => {
            return (
                <MapView.Marker
                    coordinate={{
                        latitude: job.latitude,
                        longitude: job.longitude
                    }}
                    key={job.jobkey}
                />
            )
        })
    };

    render() {
        return (
            <MapView
                style={this.props.style}
                region={this.state.region}
                initialRegion={this.state.region}
                onRegionChangeComplete={this.onRegionChangeComplete}
                provider="google"
            >
                {this.renderMarkers()}
            </MapView>
        )
    }

    shouldComponentUpdate(newProps: JobSearchMapViewProps) {
        return this.props.initialRegion !== newProps.initialRegion || this.props.jobs !== newProps.jobs;
    }
}

export default JobSearchMapView;