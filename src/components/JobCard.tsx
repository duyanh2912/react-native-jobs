import {observer} from "mobx-react";
import {Card, Text} from "react-native-elements";
import IndeedJobResultModel from "../models/IndeedJobResultModel";
import * as React from "react";
import {StyleSheet, View} from "react-native";
import {computed} from "mobx";
import CoordinateModel from "../models/CoordinateModel";
import MapView = require("react-native-maps");

interface JobCardProps {
    job: IndeedJobResultModel;
}

@observer
class JobCard extends React.Component<JobCardProps> {
    @computed get region(): CoordinateModel {
        return {
            latitude: this.props.job.latitude,
            longitude: this.props.job.longitude,
            latitudeDelta: 0.0045,
            longitudeDelta: 0.002
        }
    }

    render() {
        const job = this.props.job;

        return (
            <Card title={job.jobtitle} titleStyle={styles.title} titleNumberOfLines={1}>
                <MapView
                    scrollEnabled={false}
                    zoomEnabled={false}
                    rotateEnabled={false}
                    pitchEnabled={false}
                    style={{height: 300}}
                    provider="google"
                    cacheEnabled={true}
                    region={this.region}
                    initialRegion={this.region}
                >
                    <MapView.Marker coordinate={this.region}/>
                </MapView>
                <View style={styles.detailWrapper}>
                    <Text style={styles.detailText}>{job.company}</Text>
                    <Text style={styles.detailText}>{job.formattedRelativeTime}</Text>
                </View>
                <Text style={styles.jobSnippet}>
                    {job.snippet.replace(/(<b>|<\/b>)/g, "")}
                </Text>
            </Card>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        height: 21.3
    },
    detailWrapper: {
        flexDirection: "row",
        justifyContent: "space-around",
        marginBottom: 10,
        marginTop: 10
    },
    detailText: {
        fontWeight: "bold"
    },
    jobSnippet: {
        height: 67
    }
});

export default JobCard;