import {observer} from "mobx-react";
import * as React from "react";
import {ScrollView} from "react-native";
import SlidePage from "./SlidePage";

interface SlidesProps {
    data: SlideDataModel[]
    onComplete():void
}

@observer
class Slides extends React.Component<SlidesProps> {
    renderSlides() {
        return this.props.data.map((slide, index) => {
            return (
                <SlidePage
                    slide={slide}
                    key={index}
                    onComplete={this.props.onComplete}
                    last={index === this.props.data.length - 1}
                />)
        })
    }

    render() {
        return (
            <ScrollView
                horizontal={true}
                pagingEnabled={true}
                showsHorizontalScrollIndicator={false}
                bounces={false}
            >
                {this.renderSlides()}
            </ScrollView>
        )
    }
}

export default Slides