import {observer} from "mobx-react";
import * as React from "react";
import {Dimensions, StyleSheet, Text, View} from "react-native";
import {Button} from "react-native-elements";

interface SlidePageProps {
    slide: SlideDataModel;
    last?: boolean

    onComplete(): void;
}

@observer
class SlidePage extends React.Component<SlidePageProps> {
    renderButton() {
        if (!this.props.last) return null;
        return (
            <Button
                title="Onwards!"
                onPress={this.props.onComplete}
                raised={true}
                buttonStyle={styles.button}
                containerViewStyle={styles.buttonContainer}
            />
        )
    }

    render() {
        const slide = this.props.slide;
        return (
            <View style={[styles.container, {backgroundColor: slide.backgroundColor}]}>
                <Text style={styles.text}>{slide.text}</Text>
                {this.renderButton()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        fontSize: 30,
        textAlign: "center",
        color: "white",
        fontWeight: "bold"
    },
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        width: Dimensions.get("window").width,
        paddingHorizontal: 30,
        paddingBottom: 50
    },
    button: {
        backgroundColor: "#0288D1"
    },
    buttonContainer: {
        marginTop: 20
    }
});

export default SlidePage;