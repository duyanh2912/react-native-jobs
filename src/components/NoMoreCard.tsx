import {Card} from "react-native-elements";
import * as React from "react";

class NoMoreCard extends React.Component {
    render() {
        return (
            <Card title="No more jobs">

            </Card>
        )
    }
}

export default NoMoreCard;