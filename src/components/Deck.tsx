import {
    Animated, Dimensions, LayoutAnimation, PanResponder, PanResponderInstance, StyleSheet, UIManager, View,
    ViewStyle
} from "react-native";
import * as React from "react";

enum SwipeDirection {
    Left = "left", Right = "right"
}

interface DeckProps<T> {
    data: T[];
    containerStyle?: ViewStyle;

    keyExtractor(item: T): string;

    renderCard(item: T): Element

    renderNoMoreCard(): Element

    onSwipeLeft?(item: T): void

    onSwipeRight?(item: T): void
}

interface DeckState {
    selectedIndex: number;
}

const SCREEN_WIDTH = Dimensions.get("window").width;
const SWIPE_THRESHOLD = SCREEN_WIDTH * 0.4;
const SWIPE_SPEED = 1;
const USE_NATIVE_DRIVER = true;

class Deck<T> extends React.Component<DeckProps<T>, DeckState> {
    static defaultProps = {
        onSwipeLeft: () => {
        },
        onSwipeRight: () => {
        }
    };

    panResponder: PanResponderInstance;
    position: Animated.ValueXY;

    constructor(props: DeckProps<T>) {
        super(props);
        this.position = new Animated.ValueXY();
        this.panResponder = PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: (event, gesture) => {
                this.position.setValue({x: gesture.dx, y: 0})
            },
            onPanResponderRelease: (event, gesture) => {
                if (gesture.dx > SWIPE_THRESHOLD) {
                    this.forceSwipe(gesture.dx)
                } else if (gesture.dx < -SWIPE_THRESHOLD) {
                    this.forceSwipe(gesture.dx)
                } else {
                    this.resetPosition();
                }
            }
        });
        this.state = {selectedIndex: 0}
    }

    componentWillUpdate(nextProps: DeckProps<any>, nextState: DeckState) {
        if (!this.props.data
            || !nextProps.data
            || nextState.selectedIndex >= nextProps.data.length
            || this.state.selectedIndex >= nextState.selectedIndex)
            return;

        if (UIManager.setLayoutAnimationEnabledExperimental) {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }

        LayoutAnimation.spring();
    }

    componentWillReceiveProps(nextProps: DeckProps<T>) {
        if (nextProps.data !== this.props.data) {
            this.setState({selectedIndex: 0})
        }
    }

    forceSwipe = (currentX: number) => {
        const toValue = currentX > 0 ? SCREEN_WIDTH : -SCREEN_WIDTH;
        const duration = (SCREEN_WIDTH + (currentX > 0 ? -currentX : currentX)) / SWIPE_SPEED;

        Animated.timing(this.position.x, {
            toValue,
            duration,
            useNativeDriver: USE_NATIVE_DRIVER
        }).start(() => this.onSwipeComplete(currentX > 0 ? SwipeDirection.Right : SwipeDirection.Left))
    };

    onSwipeComplete = (direction: SwipeDirection) => {
        const {onSwipeLeft, onSwipeRight} = this.props;
        const item = this.props.data[this.state.selectedIndex];
        direction === SwipeDirection.Left ? onSwipeLeft!(item) : onSwipeRight!(item);
        this.position.setValue({x: 0, y: 0});
        this.setState({selectedIndex: this.state.selectedIndex + 1});
    };

    resetPosition = () => {
        Animated.spring(this.position.x, {
            toValue: 0,
            useNativeDriver: USE_NATIVE_DRIVER
        }).start();
    };

    getTopCardStyle = () => {
        const position = this.position;
        const rotate = (position.x as Animated.Value).interpolate({
            inputRange: [-SCREEN_WIDTH * 1.5, 0, SCREEN_WIDTH * 1.5],
            outputRange: ["-120deg", "0deg", "120deg"]
        });
        const translateX = (position.x as Animated.Value).interpolate({
            inputRange: [-1, 1],
            outputRange: [-1, 1]
        });
        return {
            // ...position.getLayout(),
            transform: [
                {translateX},
                {rotate},
                {perspective: 1000}
            ]
        };
    };

    renderCards = () => {
        if (!this.props.data || this.state.selectedIndex >= this.props.data.length) {
            return this.props.renderNoMoreCard();
        }

        const cards = this.props.data.map((item, index) => {
            if (index < this.state.selectedIndex) {
                return (
                    <Animated.View
                        key={this.props.keyExtractor(item)}
                        style={{transform: [{translateX: SCREEN_WIDTH}]}}
                    />
                );
            }

            if (index === this.state.selectedIndex) {
                return (
                    <Animated.View
                        key={this.props.keyExtractor(item)}
                        style={[this.getTopCardStyle(), styles.card, {zIndex: 10}]}
                        {...this.panResponder.panHandlers}
                    >
                        {this.props.renderCard(item)}
                    </Animated.View>
                );
            }

            return (
                <Animated.View
                    key={this.props.keyExtractor(item)}
                    style={[styles.card, {
                        top: 10 * (index - this.state.selectedIndex),
                        zIndex: -index
                    }]}
                >
                    {this.props.renderCard(item)}
                </Animated.View>
            );
        });

        return cards.reverse();
    };

    render() {
        return (
            <View style={this.props.containerStyle}>
                {this.renderCards()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    card: {
        position: "absolute",
        width: SCREEN_WIDTH
    }
});

export default Deck;