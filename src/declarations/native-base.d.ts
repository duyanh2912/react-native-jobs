import * as ReactNative from "react-native";
import {TextStyle} from "react-native";

declare module "native-base" {
    namespace NativeBase {
        interface Picker extends ReactNative.PickerProperties {
            iosHeader?: string;
            inlineLabel?: boolean;
            headerBackButtonText?: string;
            placeholder?: string;
            textStyle: TextStyle

            renderHeader(backAction: () => void): Element;
        }
    }
}