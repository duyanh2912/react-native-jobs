declare module "latlng-to-zip" {
    interface Coordinate {
        latitude: number;
        longitude: number;
    }
    function reverseGeocode(coord: Coordinate): Promise<string>;
    export = reverseGeocode;
}