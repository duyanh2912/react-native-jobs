import {CardProps} from "react-native-elements";

declare module "react-native-elements" {
    export interface CardProps {
        titleNumberOfLines?: number;
    }
}
