import {Navigator} from "react-native-navigation";

export interface ReactNativeNavigationProps {
    navigator: Navigator
}