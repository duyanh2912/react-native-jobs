export enum ScreenNames {
    Welcome = "welcomeScreen",
    Map = "mapScreen",
    Deck = "deckScreen",
    Review = "reviewScreen",
    Setting = "settingScreen"
}

export default ScreenNames