import {CodePushOptions} from "react-native-code-push";
const CodePush = require("react-native-code-push");

const CODE_PUSH_OPTIONS: CodePushOptions = {
    installMode: CodePush.InstallMode.ON_NEXT_RESUME,
    checkFrequency: CodePush.CheckFrequency.ON_APP_RESUME,
};

export {CODE_PUSH_OPTIONS};