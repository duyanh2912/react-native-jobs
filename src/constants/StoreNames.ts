enum StoreNames {
    FBAuthStore = "FB_AUTH_STORE",
    JobStore = "JOB_STORE"
}

export default StoreNames