export default interface CoordinateModel {
    longitude: number;
    latitude: number;
    longitudeDelta: number;
    latitudeDelta: number;
}