export default interface IndeedJobResultModel {
    city: string;
    company: string;
    country: string
    date: Date;
    expired: boolean;
    formattedLocation: string;
    formattedLocationFull: string;
    formattedRelativeTime: string;
    indeedApply: boolean;
    jobkey: string;
    jobtitle: string;
    language: string;
    onmousedown: string;
    snippet: string;
    source: string;
    sponsored: boolean;
    state: string;
    stations: string
    url: string;
    latitude: number;
    longitude: number;
}