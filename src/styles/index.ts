import {NavigatorStyle} from "react-native-navigation";

const navigatorStyle: NavigatorStyle = {
    navBarBackgroundColor: "#f7f7f7"
}

const DefaultStyles = {
    navigatorStyle
};

export default DefaultStyles